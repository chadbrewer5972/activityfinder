package com.example.chad.activityfinder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class PostOrSearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_or_search);
    }

    Spinner activityTyperSpinner = (Spinner) findViewById(R.id.activityTypeSpinner);

    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                                         R.array.activityChoiceArray,
                                         android.R.layout.simple_spinner_item);

    //Layout used when list of choices appears.
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
}
